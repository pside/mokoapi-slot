'use strict';

angular.module('libcodingApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute'
])
  .config(function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;﻿
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
  })
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
