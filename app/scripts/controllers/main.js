'use strict';

angular.module('libcodingApp')
    .controller('MainCtrl', function($scope, $http, $q) {
        $scope.awesomeThings = [{
            name: 'HTML5 Boilerplate',
            description: 'HTML5 Boilerplate is a professional front-end template for building fast, robust, and adaptable web apps or sites.'
        }, {
            name: 'AngularJS',
            description: 'AngularJS is a toolset for building the framework most suited to your application development.'
        }, {
            name: 'Karma',
            description: 'Spectacular Test Runner for JavaScript.'
        }];

        $scope.images = [
            "images/yeoman.png",
            "images/yeoman.png",
            "images/yeoman.png"
        ];

        var requestApi = function(date) {
            var deferred = $q.defer();
            var queryValue = date.format("YYYY-MM-DD");

            $http({
                method: "GET",
                url: "http://www.corsproxy.com/pastak.cosmio.net/mocoDB/oliveAPI/getJson.php",
                params: {
                    "date": queryValue
                }
            }).success(function(response) {
                for (var name in response) {
                    var data = response[name];
                    if ('error' in data) {
                        deferred.reject("images/yeoman.png");
                        break;
                    }

                    if (data.thumb) {
                        deferred.resolve(data.thumb);
                    } else {
                        deferred.reject("images/yeoman.png");
                    }
                }
            }).error(function() {
                deferred.reject("images/yeoman.png");
            });

            return deferred.promise;
        }

        var generateRandomDate = function() {
            var beginDate = moment("2011-04-01", "YYYY-MM-DD");
            var endDate = moment(); // now
            var duration = endDate.unix() - beginDate.unix();
            return Math.floor(Math.random() * duration) + beginDate.unix();
        }

        $scope.onClickButton = function() {
            $scope.time = generateRandomDate();


            for (var i = 0; i < 3; i++) {
                (function(){
                    var count = i;
                    requestApi(moment.unix(generateRandomDate()))
                    .then(function(result){
                        console.log(result);
                        console.log(count);
                        $scope.images[count] = result;
                    })
                    .catch(function(result){
                        console.log("ng");
                        $scope.images[count] = result;
                    });
                })();
            }
        };
    });